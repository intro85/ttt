I will not implement Log4j in this project because it is purely indicative. I just describe how I would act:
 
  - I would not use Log4j, but Log4j2 from the Lombok project
  - And to search the logs (and work with them) I would use Elasticsearch
  - The advantages of this choice: ease of setup, relatively inexpensive (it is possible to use even the free version), speed of search
  - ELK disadvantages: for a large project, you should not forget to archive logs because it can occupy decently disk space, sometimes it’s buggy