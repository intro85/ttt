package ttt;

import java.sql.Connection;
import java.sql.SQLException;

public class Foo {

    public void foo() throws SQLException, MyException {
        ConnectionManager connectionManager = new ConnectionManager();
        Connection connection = connectionManager.getConnection();
        //Doing something with DB
        connection.close();
    }

}
