package ttt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private static String urlFirstDB = "urlFirstDB";
    private static String userFirstDB = "userFirstDB";
    private static String passwordFirstDB = "passwordFirstDB";
    private static String urlSecondDB = "urlSecondDB";
    private static String userSecondDB = "userSecondDB";
    private static String passwordSecondDB = "passwordSecondDB";

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(urlFirstDB, userFirstDB, passwordFirstDB);
        } catch (SQLException e) {
            try {
                return DriverManager.getConnection(urlSecondDB, userSecondDB, passwordSecondDB);
            } catch (SQLException ex) {
                throw MyException("All db are dead!!!");
            }
        }
    }

}
